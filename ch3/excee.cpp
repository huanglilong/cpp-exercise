// exceed.cpp -- exceeding some integer limits
#include <iostream>
#include <climits>
#define ZERO 0

int main()
{
    using namespace std;
    short sam = SHRT_MAX;       // initialize a variable to max value
    unsigned short sue = sam;   // okay if variable sam already defined

    cout << "Sam has " << sam << " dollars and Sue has " << sue;
    cout << " dollars depsited." << endl
         << "Add $1 to each account." << endl << "Now ";
    sam = sam + 1;  // overflow
    sue = sue + 1;  // OK
    cout << "Sam has " << sam << " dollars and Sue has " << sue;
    cout << " dollars deoosited.\nPoor Sam!" << endl;
    sam = ZERO;
    sue = ZERO;
    cout << "Sam has " << sam << " dollars and Sue has " << sue << endl;
    cout << "Take $1 from each accout." << endl << "Now ";
    sam = sam - 1;  // OK
    sue = sue - 1;  // overflow
    cout << "Sam has " << sam << " dollars and Sue has " << sue;
    cout << " dollars deoosited." << "Lucky Sam!" << endl;
}
