// list_intialization.cpp
#include <iostream>

int main()
{
    const int code = 66;
    int x = 66;
    //char c1 {3204835};      // narrowing, not allowed
    char c2 = {66};         // allowed, because char can hold 66
    //char c4 = {x};          // not allowed, because x is variable, compiler doesn't know it's value
    x = 31325;
    char c5 = x;            // allowed by this form of initialization
    std::cout << "c5 is " << int(c5) << std::endl;
}