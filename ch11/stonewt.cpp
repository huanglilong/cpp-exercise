// stonewt.cpp -- stonewt methods
#include <iostream>
#include "stonewt.h"

using std::cout;

Stonewt::Stonewt(double lbs)
{
    stone = int(lbs) / Lbs_per_stn;
    pds_left = int(lbs) % Lbs_per_stn + lbs - int(lbs);
    pounds = lbs;
}

Stonewt::Stonewt(int stn, double lbs)
{
    stone = stn;
    pds_left = lbs;
    pounds = stn * Lbs_per_stn + lbs;
}

Stonewt::Stonewt()
{
    stone = pounds = pds_left = 0;
}

Stonewt::~Stonewt()
{

}

void Stonewt::show_stn() const
{
    cout << stone << " stone, " << pds_left << " pound\n";
}

void Stonewt::show_lbs() const
{
    cout << pounds << " pounds\n";
}

// conversion function must be class method
// must not specify a return type   --> not need, conversion type
// must have no argument            --> not need, class
Stonewt::operator double() const
{
    return pounds;
}

Stonewt::operator int() const
{
    return int(pounds + 0.5);
}