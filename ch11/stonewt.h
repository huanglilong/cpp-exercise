// stonewt.h -- definition for the stonewt class
#ifndef _STONEWT_H_
#define _STONEWT_H_

class Stonewt
{
private:
    enum{Lbs_per_stn = 14};     // pounds per stone
    int stone;
    double pds_left;
    double pounds;
public:
    // C++ any contructor that takes a single argument acts as a blueprint for converting a value
    // of that argument type to thc class type
    // Stonewt Cat; Cat = 19.6; // implicit conversion
    // explicit Stonewt(double lbs); 
    Stonewt(double lbs);           // double-to-Stonewt conversion
    Stonewt(int stn, double lbs);
    Stonewt();
    ~Stonewt();
    // conversion functions
    operator double() const; // explicit operator double() const
    operator int() const;
    void show_lbs() const;
    void show_stn() const;
};

#endif