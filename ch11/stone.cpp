// stone.cpp -- usr-defined conversion

#include <iostream>
#include "stonewt.h"

using std::cout;

int main()
{
    Stonewt incognito = 275;
    Stonewt wolfe(285.7);
    Stonewt taft(21, 8);

    cout << "The celebrity weighed ";
    incognito.show_stn();
    cout << "The detective weighed ";
    wolfe.show_stn();
    cout << "The President weighed ";
    taft.show_lbs();

    incognito = 276.8;  // implicit convertion
    taft = 325;
    cout << "After dinner, the celebrity weighed ";
    incognito.show_stn();
    cout << "After dinner, the President weighed ";
    taft.show_lbs();

    // conversion functions
    int pounds_int = int(taft);
    double pounds_double = double(taft);
    cout << "convert to dobule: " << pounds_double << std::endl;
    cout << "convert to int: " << pounds_int << std::endl;

    return 0;
}