// structur.cpp -- a simple structure
#include <iostream>

struct inflatlable 
{
    char name[20];
    float volume;
    double price;
};

enum spectrum {red, orange};

int main()
{
    using namespace std;
    inflatlable guset = {    // C++ doesn't need struct keyword, C need it
        "Glorous Gloria",
        1.88,
        29.99
    };
    // C++11 init
    inflatlable mayor {};    // init all elements to 0

    // assign one structure to another
    mayor = guset;
    cout << guset.name << endl;

    // enum
    spectrum band;
    band = red;         // only assignment operator is define for enumerations
    
}