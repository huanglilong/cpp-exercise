#include <iostream>
#include <vector>
#include <array>

int main()
{
    using namespace std;

    double a1[4] = {1.0, 32.3, 3.3, 2.2};
    vector<double> a2(4);
    array<double, 4> a3 = {3.14, 2.23, 2.51, 4.1};

    a1[-2] = 20.2;      // is ok
    a2[-2] = 20.2;      // also ok
    a3[200] = 304.0;    // also ok

    // at() member function is safe
    a2.at(-2) = 20.2;   // runtime error
    a3.at(200) =344.0;  // runtime error
    return 0;

} 