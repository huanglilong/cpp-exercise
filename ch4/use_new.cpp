// use_new.cpp -- using new operator
#include <iostream>
int main()
{
    using namespace std;
    int nights = 1001;
    int *pt = new int;
    *pt = 1001;

    cout << "night: " << nights << endl;
    cout << "pt: " << *pt << endl;
    delete pt;

    // creating dynamic array with new
    int *psome = new int [10];
    delete [] psome;
    
}