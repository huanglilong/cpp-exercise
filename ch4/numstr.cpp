// numstr.cpp -- following number input with line input
#include <iostream>
int main()
{
    using namespace std;
    cout << "what year was your house built?\n";
    int year;
    cin >> year;                        // newline in input queue
    cin.get();                          // read newline
    cout << "street address " << endl;
    char address[80];
    cin.getline(address, 80);
    cout << "Address: " << address << endl;
    cin.getline(address, 80);
    cout << "year buit: " << year << endl;
    cout << "Address: " << address << endl;
    return 0;
}