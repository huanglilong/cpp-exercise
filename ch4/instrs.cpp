// inster2.cpp -- reading more than one word with getline
#include <iostream>
int main()
{
    using namespace std;
    const int ArSize = 20;
    char name[ArSize];
    char dessert[ArSize];

    cout << "Enter your name:\n";
    cin.getline(name, ArSize);              // reads through newline
    cout << "Enter your favorite dessert:\n";
    cin.getline(dessert, ArSize);

    cout << "I have some delicious " << dessert;
    cout << " for you, " << name << ".\n";

    char test;
    // line-oriented input with get()
    cin.get(name, ArSize);
    cin.get(test);                      // or using cin.get(), read newline, overload functions, just read one character
    cin.get(dessert, ArSize);       // a whitespace in input queue
    cout << "I have some delicious " << dessert;
    cout << " for you, " << name << ".\n";


}