// strings.cpp -- storing strings in an array
#include <iostream>
#include <cstring>

int main()
{
    using namespace std;
    const int size = 15;
    char name2[size] = "C++owboy";

    cout << "name2 len " << strlen(name2) << endl;  // not include null character
}