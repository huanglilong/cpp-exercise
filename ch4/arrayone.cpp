// arrayone.cpp -- small arrays of integers
#include <iostream>
int main()
{
    using namespace std;
    int cards[4] = {3, 6, 8, 10};       // initialize when define
    int hand[4];        // okey
    //hand = cards;       // not allowed, because hand is const pointer

    float hotelTips[5] = {5.0, 2.5};    // compiler sets the remaining elements to zero;
    long totals[500] = {};             

    // C++11
    double earnings[4] {1.2e4, 1.6e4, 1.1e4, 1.7e4};
    unsigned int counts[10] = {};       // all elements set to 0
    float balances[100] = {};           // all elements set to 0

    // string
    char bird[11] = "Test.";            // Quoted string always include the terminating null character
                                        // C++ input IO read a string will add null charater automatically

    return 0;
}