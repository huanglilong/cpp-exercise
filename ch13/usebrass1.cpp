// usebrass1.cpp -- testing bank account classes
#include <iostream>
#include "brass.h"

int main()
{
    using std::cout;
    using std::endl;

    Brass Piggy("Porcelot pigg", 3405564, 4000);
    BrassPlus Hoggy("Horatio Hogg", 3485564, 3000);
    Piggy.ViewAcct();
    cout << endl;

    Hoggy.ViewAcct();
    cout << endl;

    cout << "Depositing $100 into Hogg Account:\n";
    Hoggy.Deposit(1000.00);
    cout << "New balance: $" << Hoggy.Balance() << endl;
    cout << "Piggy Withdraw $4200:\n";
    Piggy.Withdraw(4200);
    cout << "Piggy balance: $" << Piggy.Balance() << endl;
    Hoggy.Withdraw(4200);
    Hoggy.ViewAcct();
}