// express.cpp -- values of expressions
#include <iostream>

int main()
{
    using namespace std;
    int a;
    cout << "a = 100 expression's value: ";
    cout << (a=100) << endl;        // C++ using the value of an assignment expression of left

    int x,y,z;
    x = y = z = 0;                  // first init z to zero
    cout << "x ,y z: " << x << " ," << y << ", " << z << endl;

    // comma operator sneak two expression into a palce just as one expression
    int i, j;
    for(i=j=0; i< 6; i++,j++)   // comma operator, first expression evaluted first
    {
        cout << i+j << endl;
    }
    return 0;
}
