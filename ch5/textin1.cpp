// textin1.cpp -- reading chars with a while loop
#include <iostream>

int main()
{
    using namespace std;
    char ch;
    int count = 0;      // use basic input
    cout << "Enter characters; enter # to quit." << endl;

    //cin >> ch;          // ignore whitespace
    cin.get(ch);          // get all characters
    while(ch !='#')
    {
        cout << ch << endl;
        ++count;
        //cin >> ch;
        cin.get(ch);
    }
    cout << "counts:" << count << endl;
    return 0;
}