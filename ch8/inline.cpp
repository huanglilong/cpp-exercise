// inline.cpp -- using an inline function
#include <iostream>

#define SQUARE(X) X*X

// an inline function definition
inline double square(double x)
{
    return x*x;
}

int main()
{
    using namespace std;
    double a, b;
    double c;

    a = square(5.0);
    b = square(4.5 + 7.5);
    c = SQUARE(4.5 + 7.5);      //  c = 4.5 + 7.5 * 4.5 + 7.5
    cout << "a: " << a << endl;
    cout << "b: " << b << endl;
    cout << "c: " << c << endl;
    return 0;
}