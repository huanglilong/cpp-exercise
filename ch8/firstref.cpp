// firstref.cpp -- defining and using a reference

// refernece is rather like a const pointer
#include <iostream>

const int & test_ref(int &a);

int main()
{
    using namespace std;
    int rats = 101;
    int & rodents = rats;       // define and must init
    rodents++;
    cout << "rats = " << rats;
    cout << ", rodents = " << rodents << endl;

    cout << "rats address = " << &rats;
    cout << ", rodents address = " << &rodents << endl;
    //test_ref(rats) = rodents;        // assignment not allow
    return 0;
}

const int & test_ref(int &a)
{
    return a = a*a;
}