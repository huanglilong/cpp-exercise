// funtemp.cpp -- using a function template
#include <iostream>
// function template prototype
template <typename T>
void Swap(T &a, T &b);

template <typename T>
void Swap(T *a, T *b, int n);   // overload

int main()
{
    using namespace std;
    int i = 10; 
    int j = 20;
    cout << "i, j = " << i << ", " << j << endl;
    cout << "Using compiler-generated int Swapper:" << endl;
    Swap(i, j); // compiler generate void Swap(int &, int &)
    cout << "Now i, j = " << i << ", " << j << endl;

    double x = 34.5;
    double y = 90.2;
    cout << "x, y = " << x << ", " << y << endl;
    cout << "Using compiler-generated double Swapper:" << endl;
    Swap(x, y); // compiler generate void Swap(double &, double &)
    cout << "Now x, y = " << x << ", " << y << endl;

    double xx = 4.4;
    decltype((xx)) r2 = xx;     // double &
    decltype(xx) r3 = xx;
    cout << "xx's address: " << &xx << endl;
    cout << "r2's address: " << &r2 << endl;
    cout << "r3's address: " << &r3 << endl;
    return 0;
}

template <typename T>
void Swap(T &a, T &b)
{
    T temp;
    temp = a;
    a = b;
    b = temp;
}

template <typename T>
void Swap(T *a, T *b, int n)
{
    T temp;
    for(int i = 0; i < n; i++)
    {
        temp = a[i];
        a[i] = b[i];
        b[i] = temp;
    }
}