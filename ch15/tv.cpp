// tv.cpp methods for Tv class 
#include <iostream>
#include "tv.h"

bool Tv::volup()
{
    if(volume < MaxVal)
    {
        volume++;
        return true;
    }
    else
    {
        return false;
    }
}

bool Tv::voldown()
{
    if(volume > MaxVal)
    {
        volume--;
        return true;
    }
    else
    {
        return false;
    }
}

void Tv::chanup()
{
    if(channel < maxchannel)
    {
        channel++;
    }
    else
    {
        channel = 1;
    }
}

void Tv::chandown()
{
    if(channel > 1)
    {
        channel--;
    }
    else
    {
        channel = maxchannel;
    }
}

void Tv::settings() const
{
    using std::cout;
    using std::endl;
    cout <<"TV is " << (state == Off ? "Off" : "On") << endl;
    cout <<"Mode = "
        << (mode == Antenna ? "Antenna" : "Cable") << endl;
    cout << "Input = " << (input == TV ? "TV" : "DVD") << endl;
}