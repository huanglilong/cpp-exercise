// exc_mean.h -- exception classes for mean(), gmean()
#ifndef _EXC_MEAN_H_
#define _EXC_MEAN_H_

#include <iostream>

class bad_hmean
{
private:
    double v1;
    double v2;
public:
    bad_hmean(double a = 0, double b = 0)
    : v1(a), v2(b) {}
    void msg();
};

inline void bad_hmean::msg()
{
    std::cout << "invaild argument: a = -b\n";
}

#endif