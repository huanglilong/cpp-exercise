// error1.cpp -- using the abort() function
#include <iostream>
#include <cstdlib>

double hmean(double a, double b);

int main()
{
    double x, y, z;
    std::cout << "Enter two numers: ";
    while(std::cin >> x >> y)
    {
        try
        {
            z = hmean(x,y);
        }
        catch(const char *s)
        {
            std::cout << s << std::endl;
        }
        
    }
    return 0;
}

double hmean(double a, double b)
{
    if(a == -b)
    {
        throw "bad hmean(), arguments: a = -b not allowed";
        //std::cout << "untenable arguments to hmean()\n";
        //std::abort();  // display msg
        //std::exit(-1);   // flush file buffers
    }
    return 2.0 * a * b / (a + b);
}