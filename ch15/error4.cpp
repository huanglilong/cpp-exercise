// error4.cpp -- using exception classes
#include <iostream>
#include <cmath>
#include "exc_mean.h"

double hmean(double a, double b);

int main()
{
    double x, y, z;
    while(std::cin >> x >> y)
    {
        try
        {
            z = hmean(x,y);            
        }
        catch(bad_hmean &bg)
        {
            bg.msg();
            std::cout << "Try again.\n";
            continue;
        }
    }
    return 0;
}

double hmean(double a, double b)
{
    if(a == -b)
    {
        throw bad_hmean(a,b);
        return 2.0 * a * b / (a + b);
    }
}

