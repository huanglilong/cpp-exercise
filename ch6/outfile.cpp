// outfile.cpp -- writing to a file
#include <iostream>
#include <fstream>

int main()
{
    using namespace std;

    char automobile[50];
    int year;
    double a_price;
    double d_price;

    ofstream outFile;
    outFile.open("carinfo.txt");
    cout << "Enter the make and model of automobile: ";
    cin.getline(automobile, 50);
    cout << "Enter the model year: ";
    cin >> a_price;
    d_price = 0.913 * a_price;

    // display information on screen with cout
    cout << fixed;
    cout.precision(2);
    cout.setf(ios_base::showpoint);
    cout << "make and model: " << automobile << endl;
    cout << "Year: " << year << endl;
    cout << "WAs asking $" << a_price << endl;
    cout << "Now asking $" << d_price << endl;

    // using outFile object
    outFile << fixed;
    outFile.precision(2);
    outFile.setf(ios_base::showpoint);
    outFile << "make and model: " << automobile << endl;
    outFile << "Year: " << year << endl;
    outFile << "WAs asking $" << a_price << endl;
    outFile << "Now asking $" << d_price << endl;
    outFile.close();
    return 0;
}