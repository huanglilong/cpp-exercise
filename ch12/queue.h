// queue.h -- interface for a queue
#ifndef _QUEUE_H_
#define _QUEUE_H_

// this queue will contain customer items
class Customer
{
private:
    long arrive;
    int processtime;
public:
    Customer() { arrive = processtime = 0; }
    void set(long when);
    long when() const { return arrive;}
    int ptime() const { return processtime; }
};

typedef Customer Item;

class Queue
{
private:
    // class socpe definitions
    // Node is a nested structure definition local to this class
    struct Node { Item item; struct Node *next;};
    enum {Q_SIZE = 10};
    // private class members
    Node *front;
    Node *rear;
    int items;              // current number of items in Queue
    const int qsize;        // maximum number of items in Queue
    // preemptive definitions to prevent public copying
    Queue(const Queue &q) : qsize(0) {}                 // copy constructor
    Queue & operator=(const Queue &q){ return *this; }  // assignment opreator
public:
    Queue(int qs = Q_SIZE); 
    ~Queue();
    bool isempty() const;
    bool isfull() const;
    int queuecount() const;
    bool enqueue(const Item &item);
    bool dequeue(Item &item);
};

#endif