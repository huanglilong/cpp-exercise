// queue.cpp -- Queue and Customer methods
#include <cstdlib>
#include "queue.h"

// qsize is const type, must init in member initialization list
Queue::Queue(int qs): qsize(qs)
{
    front = rear = nullptr;
}

Queue::~Queue()
{
    Node *temp;
    while(front != nullptr)
    {
        temp = front;
        front = front->next;    // point to next item
        delete temp;
    }
}

bool Queue::isempty() const
{
    return items == 0;
}

bool Queue::isfull() const
{
    return items == qsize;
}

int Queue::queuecount() const
{
    return items;
}

bool Queue::enqueue(const Item &item)
{
    if(isfull())
    {
        return false;
    }
    Node *add = new Node;
    add->item = item;
    add->next = nullptr;  // add new item to queue
    items++;
    if(front == nullptr)
    {
        front = add;        // empty queue
    }
    else
    {
        rear->next = add;
    }
    rear = add;
    return true;

}

bool Queue::dequeue(Item &item)
{
    if(front == nullptr) // or isempty
    {
        return false;
    }
    Node *temp = front;
    items++;
    item = front->item;
    front = front->next;
    delete temp;
    if(items == 0)
    {
        rear = nullptr;
    }

    return true;
}

void Customer::set(long when)
{
    processtime = std::rand() % 3 + 1;
    arrive = when;
}